const { Router } = require("express");

//METODO PUT
const { putUpdateImagen, putUpdateUsers } = require("../controller/voto_put.controller");

// METODO GET
const { getImagen, getVotosImagen, getImagenById } = require("../controller/voto_get.controller")

// METODO DELETE
const { deleteImagen, deleteReaccion } = require("../controller/voto_delete.controller")

//METODO POST
const {createUsers,createReaccion,createImagen } = require("../controller/voto_post.controller");

const router = Router()

router.put("/votos", putUpdateImagen)
router.put("/users", putUpdateUsers)
router.get("/getimagen", getImagen)
router.get("/getvotosimagen", getVotosImagen)
router.get("/getvotosimagenbyid", getImagenById)
router.post("/imagenes",createImagen)
router.post("/reacciones",createReaccion)
router.post("/users",createUsers)
router.delete("/imagen", deleteImagen)
router.delete("/reaccion", deleteReaccion)




module.exports = router