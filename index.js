
const express = require('express');

const app = express()

const bodyParse = require ('body-parser')

const cors = require('cors')


//milddlewears

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors({ origin: true, credentials: true }));


//routes

app.use(require('./routes/index'))


//execution server web
app.get('/', (req,res) => {res.send('Bienvenidos al API REST IMAGENES!!')})
app.listen(4000)
console.log('server running in http://localhost:4000')

