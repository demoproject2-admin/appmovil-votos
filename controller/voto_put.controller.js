
const { db } = require("../cnn")

const putUpdateImagen = async (req, res) => {

    const { id_img,id_user,nombre_img,url_img } = req.query;
    const response = await db.any(`UPDATE public.imagen
	SET  id_user=$2, nombre_img=$3, url_img=$4
	WHERE id_img=$1;`, [id_img,id_user,nombre_img,url_img]);

  

    console.log(response)
    res.json({
        message: 'Imagen actualizada correctamente',
        body: {

            imagen: {id_img,id_user,nombre_img,url_img}

            

        }
    })
}

const putUpdateUsers = async (req, res) => {

    const { id_user,nombre_user,pass_user } = req.query;
    const response = await db.any(`UPDATE public.users
	SET nombre_user=$2, pass_user=$3
	WHERE id_user=$1;`, [id_user,nombre_user,pass_user]);

  

    console.log(response)
    res.json({
        message: 'Usuario actualizado correctamente',
        body: {

            users: {id_user,nombre_user,pass_user}

            

        }
    })
}

module.exports = {

    putUpdateImagen,
    putUpdateUsers,

}
