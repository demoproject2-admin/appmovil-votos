const { db } = require("../cnn")

const deleteImagen = async (req, res) => {
    const { id_img } = req.query;
    const response = await db.any(`DELETE FROM public.imagen
	WHERE id_img = $1;`, [id_img]);
    res.json({
        message: 'Imagen borrada correctamente',
        body: {
            imagen: { id_img }
        }
    })
}

const deleteReaccion = async (req, res) => {
    const { id_reac } = req.query;
    const response = await db.any(`DELETE FROM public.reaccion
	WHERE id_reac = $1;`, [id_reac]);
    res.json({
        message: 'Reaccion borrada correctamente',
        body: {
            reaccion: { id_reac }
        }
    })
}


module.exports = {
    deleteImagen,
    deleteReaccion
}