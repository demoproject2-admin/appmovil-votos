const { db } = require("../cnn")

const createUsers = async (req, res) => {
    const { nombre_user, pass_user } = req.query;
    const response = await db.any(`INSERT INTO users(nombre_user, pass_user)
     VALUES ($1, $2)`, [nombre_user, pass_user]);
    //console.log(response)
    res.json({
        message: 'Usuario creado correctamente',
        body: {
            users: {nombre_user, pass_user}
        }
    })
}
const createReaccion = async (req, res) => {
    const { id_img, id_user, estado_reac} = req.query;
    const response = await db.any(`INSERT INTO reaccion(id_img, id_user, estado_reac)
     VALUES ($1,$2,$3)`, [id_img, id_user, estado_reac]);
    //console.log(response)
    res.json({
        message: 'Reaccion creada correctamente',
        body: {
            reacciones: {id_img, id_user, estado_reac}
        }
    })
}
const createImagen = async (req, res) => {
    const {id_user, nombre_img, url_img} = req.query;
    const response = await db.any(`INSERT INTO imagen(id_user, nombre_img, url_img)
     VALUES ($1, $2, $3)`, [id_user, nombre_img, url_img]);
    //console.log(response)
    res.json({
        message: 'Imagen creada correctamente',
        body: {
            imagenes: {id_user, nombre_img, url_img}
        }
    })
}

module.exports={
    createUsers,
    createReaccion,
    createImagen
}