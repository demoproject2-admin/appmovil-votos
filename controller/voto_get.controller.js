const { db } = require("../cnn")

//pizza

const getImagen=async(req, res)=>{
    const response = await db.any('select * from imagen')
    res.json(response)
}

const getVotosImagen=async(req, res)=>{
    const response = await db.any('select COUNT(r.id_reac) as Nro_Likes, i.nombre_img from imagen i INNER JOIN reaccion r ON i.id_img=r.id_img WHERE r.estado_reac = True GROUP BY i.id_img')
    res.json(response)
}

const getImagenById = async (req, res) => {
    const id = req.params.id;
    const response = await db.any('select COUNT(r.id_reac) as Nro_Likes, i.nombre_img from imagen i INNER JOIN reaccion r ON i.id_img=r.id_img WHERE r.estado_reac = True and i.id_img = $1 GROUP BY i.nombre_img ;', [id]);
    res.json(response)
}

module.exports={
    getImagen,
    getVotosImagen,
    getImagenById
}